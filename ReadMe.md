# When viewing the Jupyter Notebook (.ipynb file) on your right change from the default file viewer  to ipython notebook

A simple way to create a dataset using an API, especially, when a data scientist wants to be in control of the dataset they are using.